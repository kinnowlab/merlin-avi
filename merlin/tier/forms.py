from django import forms
from tier.models import Tier, Price
from django import forms
from .models import Register


class LoginForm(forms.ModelForm):
	class Meta:
		model = Register
		fields = ('email', 'password')


class SignUpForm(forms.ModelForm):
	class Meta:
		model = Register
		fields = ('name', 'email', 'password')


class TierForm(forms.ModelForm):

	benifits = forms.CharField(widget=forms.TextInput(attrs={'id': 'tier-benefits'}))

	annual_billing = forms.BooleanField(widget=forms.CheckboxInput(attrs={'id':'price-annual-billing'}))


	class Meta:
		model = Price
		fields = '__all__'




from django.db import models
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin
import uuid
from django.utils import timezone



class Register(AbstractUser):
    class Meta:
    	db_table = "user"

    id = models.UUIDField(primary_key=True, default = uuid.uuid4, editable = False)
    firebase_uid = models.CharField(max_length=100, null=True)
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=100)
    created_dt = models.DateField(default=timezone.now)

    def __str__(self):
        return f'{self.email}'


class Page(models.Model):

	id = models.UUIDField(primary_key=True, default = uuid.uuid4, editable = False)
	created_dt = models.DateField(default=timezone.now)
	link = models.CharField(max_length=100, null=True)
	description	 = models.CharField(max_length=100, null=True)
	profile_photo = models.ImageField(null=True)
	cover_photo = models.ImageField(null=True)
	user = models.ForeignKey(Register, on_delete=models.CASCADE)


class Post(models.Model):
	id = models.AutoField(primary_key=True)

class Tier(models.Model):

	id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
	name=models.CharField(max_length=100, default="none")
	benifits = models.CharField(max_length=20)
	annual_billing = models.BooleanField(default=False)
	page = models.ForeignKey(Page, on_delete=models.CASCADE)
	post = models.ForeignKey(Post, on_delete=models.CASCADE, blank=True, null=True)

	def __str__(self):
		return self.benifits

class Price(models.Model):
	id = models.IntegerField(primary_key=True)
	unit_amount = models.CharField(max_length=100)
	recurring_interval = models.CharField(max_length=50)
	tier = models.ForeignKey(Tier, on_delete=models.CASCADE)
	currency = models.CharField(max_length=5)

class Product(models.Model):
	id = models.CharField(primary_key=True, max_length=100)
	name = models.CharField(max_length=100)
	description = models.CharField(max_length=100)
